---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements

# Embedded files
7757d8dd9ba499e4108d03ab454a65078923300a: [[Pasted Image 20230218111342_037.png]]

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://excalidraw.com",
	"elements": [
		{
			"type": "image",
			"version": 105,
			"versionNonce": 2143783257,
			"isDeleted": false,
			"id": "T1kuHlFz9VG5gRuKcu2QP",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -159.90112120793913,
			"y": -313.7135141908733,
			"strokeColor": "transparent",
			"backgroundColor": "transparent",
			"width": 318.3316829030107,
			"height": 725.0888332790798,
			"seed": 1521591833,
			"groupIds": [],
			"roundness": null,
			"boundElements": [],
			"updated": 1676706217997,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "7757d8dd9ba499e4108d03ab454a65078923300a",
			"scale": [
				1,
				1
			]
		},
		{
			"type": "ellipse",
			"version": 88,
			"versionNonce": 538994073,
			"isDeleted": false,
			"id": "IaE0xyYNCTHXyIhLmVqtk",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "dashed",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -160.67645622702207,
			"y": -83.90108489990234,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 236.2352797564339,
			"height": 63.999992819393356,
			"seed": 1767052471,
			"groupIds": [],
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1676706241873,
			"link": null,
			"locked": false
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#c92a2a",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "dashed",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 593.264705882353,
		"scrollY": 434.9158212998335,
		"zoom": {
			"value": 0.8499999999999999
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"colorPalette": {},
		"currentStrokeOptions": null,
		"previousGridSize": null
	},
	"files": {}
}
```
%%