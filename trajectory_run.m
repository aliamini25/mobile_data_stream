function trajLog = trajectory_run(trajectory)

angVelLog = zeros(trajectory.TimeOfArrival(end) * trajectory.SampleRate,3);
orientationLog = zeros(trajectory.TimeOfArrival(end) * trajectory.SampleRate,1,'quaternion');
accelerationLog = zeros(trajectory.TimeOfArrival(end) * trajectory.SampleRate,3);
velocityLog = zeros(trajectory.TimeOfArrival(end) * trajectory.SampleRate,3);
positionLog = zeros(trajectory.TimeOfArrival(end) * trajectory.SampleRate,3);


reset(trajectory)
count = 1;

while ~isDone(trajectory)
   [positionLog(count,:),orientationLog(count),velocityLog(count,:),accelerationLog(count,:),angVelLog(count,:)] = trajectory();
   %plot(currentPosition(1),currentPosition(2),'bo')
   %pause(trajectory.SamplesPerFrame/trajectory.SampleRate)
   count = count + 1;
end

timeLog = [0:1/trajectory.SampleRate:trajectory.TimeOfArrival(end)]';
timeLog = timeLog(2:end);

trajLog.time = timeLog;
trajLog.angVel = angVelLog;
trajLog.orientation = orientationLog;
angles = eulerd(orientationLog,'ZYX','frame');
trajLog.euler.phi = angles(:,3);
trajLog.euler.theta = angles(:,2);
trajLog.euler.psi = angles(:,1);
trajLog.FreeAccl = accelerationLog;
trajLog.vel = velocityLog;
trajLog.pos = positionLog;


reset(trajectory)



end