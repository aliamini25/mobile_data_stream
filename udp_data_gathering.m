%% In the name of God
% main part of the program is a callback inside the udp packet. exactly
% when a termination of an Ascii message triggered.
% Only use src.UserData field to store and load the data in the callback.
% All other things are lost in each step of the program.
% src.UserData.IMU holds all the IMU data during real-time use.
% At last, all the IMU offline data will be in a structure with the name "IMU".
clear
%% configuration
duration_minute = 1;
listen_port_number = 2055;
sensors_order = {'time','ax','ay','az','mx','my','mz','gx','gy','gz','lat','lon','alt'}; %only change the order of the sensors.
simulation_mode = false; %if it's a simulation from log imu data, make this variable true.

if simulation_mode
    file = uigetfile('*.mat',"select an imu_data");
    load(file,'sim_data');
end
%% making port
clear udp_port u_send
if ~(exist('udp_port','var'))
    udp_port = udpport('LocalPort',listen_port_number);
    configureTerminator(udp_port,"CR/LF");
    udp_port.UserData = [];
    udp_port.UserData.channels = index_def(sensors_order);
    udp_port.UserData.sim_mode = simulation_mode;
    configureCallback(udp_port,"terminator",@IMU_read_callback);
else
    udp_port.UserData = [];
end

%% initialization
flush(udp_port);
udp_port.UserData.Counter = 0;
udp_port.UserData.Data = [];
udp_port.UserData.Estimation = [];
animated_line_plot_config(udp_port);





%% Gathering duration
if simulation_mode
    publish_sensor_data_from_file(sim_data, listen_port_number, udp_port);    
else
    pause(duration_minute * 60);
end


%% making IMU Data
result.IMU = udp_port.UserData.IMU;
result.GPS = udp_port.UserData.GPS;
result.Estimation = udp_port.UserData.Estimation;
if simulation_mode
    result.GroundTruth = sim_data.GT;
else
    result.GroundTruth = [];
end


%% post simulation works
clear udp_port
%plot_imu_data(result.IMU);

if simulation_mode
    compare_estimated_and_ground_truth(result);
end










%% functions

function chans = index_def(sensors_order)
    chans = zeros(13,1);
    chans(1) = find(contains(sensors_order,'time'));
    chans(2) = find(contains(sensors_order,'ax'));
    chans(3) = find(contains(sensors_order,'ay'));
    chans(4) = find(contains(sensors_order,'az'));
    chans(5) = find(contains(sensors_order,'mx'));
    chans(6) = find(contains(sensors_order,'my'));
    chans(7) = find(contains(sensors_order,'mz'));
    chans(8) = find(contains(sensors_order,'gx'));
    chans(9) = find(contains(sensors_order,'gy'));
    chans(10) = find(contains(sensors_order,'gz'));
    chans(11) = find(contains(sensors_order,'lat'));
    chans(12) = find(contains(sensors_order,'lon'));
    chans(13) = find(contains(sensors_order,'alt'));
    
end


function IMU_read_callback(src,~)
    
    data_wrap();
    realtime_IMU_plot();
    %realtime_estimation();
    
    
% functions
    function data_wrap()
        data = readline(src);
        temp = textscan(data,'%f','Delimiter',',');
        src.UserData.Counter = src.UserData.Counter + 1;
        src.UserData.Data(src.UserData.Counter,:) = transpose(temp{:});
        data_count = src.UserData.Counter;
        data_vector = src.UserData.Data;
        channels = src.UserData.channels;
        time = (data_vector(data_count,channels(1))-data_vector(1,channels(1)))*10^-3;
        acc_x = data_vector(data_count,channels(2));
        acc_y = data_vector(data_count,channels(3));
        acc_z = data_vector(data_count,channels(4));
        mag_x = data_vector(data_count,channels(5));
        mag_y = data_vector(data_count,channels(6));
        mag_z = data_vector(data_count,channels(7));
        gyro_x = data_vector(data_count,channels(8));
        gyro_y = data_vector(data_count,channels(9));
        gyro_z = data_vector(data_count,channels(10));
        lat = data_vector(data_count,channels(11));
        lon = data_vector(data_count,channels(12));
        alt = data_vector(data_count,channels(13));
        src.UserData.IMU.time(data_count,:) = time;
        src.UserData.IMU.acc(data_count,:) = [acc_x,acc_y,acc_z];
        src.UserData.IMU.gyro(data_count,:) = [gyro_x,gyro_y,gyro_z];
        src.UserData.IMU.mag(data_count,:) = [mag_x,mag_y,mag_z];
        src.UserData.GPS.geoLoc(data_count,:) = [lat,lon,alt];
        src.UserData.GPS.relLoc(data_count,:) = relativeLoc([lat,lon,alt],src.UserData.GPS.geoLoc(1,:));      
    end

    function L = relativeLoc(pos,pos0)
        L = zeros(1,3);
        EarthR = 6371000;
        L(1) = EarthR * pi/180*(pos(1)-pos0(1));
        L(2) = EarthR*cosd(pos0(1))*pi/180*(pos(2)-pos0(2));
        L(3) = pos(3)-pos0(3);   
    end

    function realtime_IMU_plot()
        i = src.UserData.Counter;
        addpoints(src.UserData.anim_line,src.UserData.IMU.time(i),src.UserData.IMU.acc(i,1));
        addpoints(src.UserData.anim_line2,src.UserData.IMU.time(i),src.UserData.IMU.acc(i,2));
        addpoints(src.UserData.anim_line3,src.UserData.IMU.time(i),src.UserData.IMU.acc(i,3));
        addpoints(src.UserData.anim_line4,src.UserData.IMU.time(i),src.UserData.IMU.gyro(i,1));
        addpoints(src.UserData.anim_line5,src.UserData.IMU.time(i),src.UserData.IMU.gyro(i,2));
        addpoints(src.UserData.anim_line6,src.UserData.IMU.time(i),src.UserData.IMU.gyro(i,3));
        addpoints(src.UserData.anim_line7,src.UserData.IMU.time(i),src.UserData.IMU.mag(i,1));
        addpoints(src.UserData.anim_line8,src.UserData.IMU.time(i),src.UserData.IMU.mag(i,2));
        addpoints(src.UserData.anim_line9,src.UserData.IMU.time(i),src.UserData.IMU.mag(i,3));
        
        drawnow limitrate
    end

    function realtime_estimation()
        % All the information you need for estimator are in below variables:
        i = src.UserData.Counter;% counter of steps
        g = src.UserData.IMU.gyro(i,:)'; % gyro data (3x1 vector)
        a = src.UserData.IMU.acc(i,:)'; %accelerometer data (3x1 vector)
        m = src.UserData.IMU.mag(i,:)'; %magnetometer data (3x1 vector)
        p = src.UserData.GPS.relLoc(i,:)';%position data from GPS
        
        % in each time step, below fileds must be estimated. (replace zeros
        % with appropriate estimation)
        % Note that if you don't need to estimate velocity and position,
        % leave them with their zero values.
        % Note that initialization of variables are done for you.
        % if you need any other information to the next steps, you can make
        % new variables in src.UserData structure.
        
        if (i == 1) %initialization.
            estimation_initial_config();          
            src.UserData.Estimation.phi(i,1) = src.UserData.init.orientation(1);
            src.UserData.Estimation.theta(i,1) = src.UserData.init.orientation(2);
            src.UserData.Estimation.psi(i,1) = src.UserData.init.orientation(3);
            src.UserData.Estimation.velocity(i,:) = src.UserData.init.vel;
            src.UserData.Estimation.position(i,:) = src.UserData.init.pos;          
        else % runtime
            % change the code below 
            src.UserData.Estimation.phi(i,1) = 0; %roll angle estimation
            src.UserData.Estimation.theta(i,1)= 0; %pith angle estimation
            src.UserData.Estimation.psi(i,1) = 0; %yaw angle estimation
            src.UserData.Estimation.velocity(i,:) = [0,0,0]; %velocity estimation
            src.UserData.Estimation.position(i,:) = [0,0,0]; %position estimation
            
     
            
            
            
            
            
            
            
            
            
        end

        estimation_plot_update();
        
           
        function estimation_initial_config()
            src.UserData.Estimation.Fig = figure();
            set(src.UserData.Estimation.Fig,'defaultLegendAutoUpdate','off');
            figure(src.UserData.Estimation.Fig);
            
            tcl = tiledlayout(3,1);
            nexttile(tcl)
            title("Euler Angles Estimation Result");
            src.UserData.Estimation.animline1 = animatedline('color',[0    0.4470    0.7410],'DisplayName','Phi','LineWidth',1);
            ylabel('Roll Angle(deg)');
            
            nexttile(tcl)
            src.UserData.Estimation.animline2 = animatedline('color',[0.8500    0.3250    0.0980],'DisplayName','Theta','LineWidth',1);
            ylabel('Pitch Angle (deg)');
            
            nexttile(tcl)
            src.UserData.Estimation.animline3 = animatedline('color',[0.9290    0.6940    0.1250],'DisplayName','Psi','LineWidth',1);
            ylabel('Yaw Angle (deg))');
            xlabel('time(s)');
            
            if ~src.UserData.sim_mode
                angles = attitude_measurement(a,m);
                src.UserData.init.orientation(1) = angles(1);
                src.UserData.init.orientation(2) = angles(2);
                src.UserData.init.orientation(3) = angles(3);
                src.UserData.init.vel = [0,0,0];
                src.UserData.init.pos = p';
            end
            
               
        end
        
        function estimation_plot_update()
            addpoints( src.UserData.Estimation.animline1,src.UserData.IMU.time(i),src.UserData.Estimation.phi(i,1)*180/pi);
            addpoints( src.UserData.Estimation.animline2,src.UserData.IMU.time(i),src.UserData.Estimation.theta(i,1)*180/pi);
            addpoints( src.UserData.Estimation.animline3,src.UserData.IMU.time(i),src.UserData.Estimation.psi(i,1)*180/pi);
            drawnow limitrate
        end
        
    end
end



function plot_imu_data(IMU)
    f1 = figure;
    tcl = tiledlayout(3,1);
    nexttile(tcl)
    plot(IMU.time,IMU.acc,'LineWidth',1)
    ylabel('acceleration (m/s2)');
    nexttile(tcl)
    plot(IMU.time,IMU.gyro,'LineWidth',1)
    ylabel('angular rate (rad/s)');
    nexttile(tcl)
    plot(IMU.time,IMU.mag,'LineWidth',1)
    ylabel('magnetic field (uT)');
    xlabel('time(s)');
    hL = legend({'x','y','z'},'Orientation','horizontal'); 
    hL.Layout.Tile = 'South';
    linkaxes(findobj(f1,'type','axes'),'x');
    
end

function animated_line_plot_config(udp_port)
    udp_port.UserData.Fig = figure;
    set(udp_port.UserData.Fig,'defaultLegendAutoUpdate','off');
    figure(udp_port.UserData.Fig);
    tcl = tiledlayout(3,1);
    nexttile(tcl)
    udp_port.UserData.anim_line = animatedline('color',[0    0.4470    0.7410],'DisplayName','x','LineWidth',1);
    udp_port.UserData.anim_line2 = animatedline('color',[0.8500    0.3250    0.0980],'DisplayName','y','LineWidth',1);
    udp_port.UserData.anim_line3 = animatedline('color',[0.9290    0.6940    0.1250],'DisplayName','z','LineWidth',1);
    ylabel('acceleration (m/s2)');

    nexttile(tcl)
    udp_port.UserData.anim_line4 = animatedline('color',[0    0.4470    0.7410],'DisplayName','x','LineWidth',1);
    udp_port.UserData.anim_line5 = animatedline('color',[0.8500    0.3250    0.0980],'DisplayName','y','LineWidth',1);
    udp_port.UserData.anim_line6 = animatedline('color',[0.9290    0.6940    0.1250],'DisplayName','z','LineWidth',1);
    ylabel('angular rate (rad/s)');

    nexttile(tcl)
    udp_port.UserData.anim_line7 = animatedline('color',[0    0.4470    0.7410],'DisplayName','x','LineWidth',1);
    udp_port.UserData.anim_line8 = animatedline('color',[0.8500    0.3250    0.0980],'DisplayName','y','LineWidth',1);
    udp_port.UserData.anim_line9 = animatedline('color',[0.9290    0.6940    0.1250],'DisplayName','z','LineWidth',1);
    ylabel('magnetic field (uT)');
    xlabel('time(s)');
    %hL = legend([udp_port.UserData.anim_line7,udp_port.UserData.anim_line8,udp_port.UserData.anim_line9],'Orientation','horizontal');
    hL = legend({'x','y','z'},'Orientation','horizontal');
    hL.Layout.Tile = 'South';
end

function  publish_sensor_data_from_file(sim_data,port_number,udp_recieve)

udp_recieve.UserData.init.orientation = [sim_data.GT.euler.phi(1),sim_data.GT.euler.theta(1),sim_data.GT.euler.psi(1)];
udp_recieve.UserData.init.vel = sim_data.GT.vel(1,:);
udp_recieve.UserData.init.pos = sim_data.GT.pos(1,:);
dt = sim_data.time(2)-sim_data.time(1);
u_send = udpport;
configureTerminator(u_send,"CR/LF");
count = 1;
formatSpec = '%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%f,%f,%f';
while count <= length(sim_data.time)
    if count~=1
        t_end_loop = toc(t_init_loop);
        while (t_end_loop < dt)
            t_end_loop = toc(t_init_loop);
        end
    end
    t_init_loop = tic;
    temp = num2str([sim_data.time(count)*10^3,sim_data.acc(count,:),sim_data.mag(count,:),sim_data.gyro(count,:),sim_data.GPS(count,:)],formatSpec);
    writeline(u_send,temp,"127.0.0.1",port_number);
    count = count + 1;
end
end

function compare_estimated_and_ground_truth(result)
    f1 = figure;
    tcl = tiledlayout(3,1);
    nexttile(tcl)
    plot(result.IMU.time,result.Estimation.phi*180/pi,'-b',result.GroundTruth.time,result.GroundTruth.euler.phi,'--r');
    title('Euler Angles Estimation');
    ylabel('Roll Angle(deg)');
    nexttile(tcl)
    plot(result.IMU.time,result.Estimation.theta*180/pi,'-b',result.GroundTruth.time,result.GroundTruth.euler.theta,'--r');
    ylabel('Pitch Angle(deg)');
    nexttile(tcl)
    plot(result.IMU.time,result.Estimation.psi*180/pi,'-b',result.GroundTruth.time,result.GroundTruth.euler.psi,'--r');
    ylabel('Yaw Angle(deg)');
    xlabel('time(s)');
    hL = legend({'Estimation','Ground Truth'},'Orientation','horizontal'); 
    hL.Layout.Tile = 'South';
    linkaxes(findobj(f1,'type','axes'),'x');
    
    f1 = figure;
    tcl = tiledlayout(3,1);
    nexttile(tcl)
    plot(result.IMU.time,result.Estimation.velocity(:,1),'-b',result.GroundTruth.time,result.GroundTruth.vel(:,1),'--r');
    title('Velocity Estimation');
    ylabel('V North(m/s)');
    nexttile(tcl)
    plot(result.IMU.time,result.Estimation.velocity(:,2),'-b',result.GroundTruth.time,result.GroundTruth.vel(:,2),'--r');
    ylabel('V East(m/s)');
    nexttile(tcl)
    plot(result.IMU.time,result.Estimation.velocity(:,3),'-b',result.GroundTruth.time,result.GroundTruth.vel(:,3),'--r');
    ylabel('V Down(m/s)');
    xlabel('time(s)');
    hL = legend({'Estimation','Ground Truth'},'Orientation','horizontal'); 
    hL.Layout.Tile = 'South';
    linkaxes(findobj(f1,'type','axes'),'x');
    
    f1= figure;
    tcl = tiledlayout(3,1);
    nexttile(tcl)
    plot(result.IMU.time,result.Estimation.position(:,1),'-b',result.GroundTruth.time,result.GroundTruth.pos(:,1),'--r');
    title('Position Estimation');
    ylabel('Location North (m)');
    nexttile(tcl)
    plot(result.IMU.time,result.Estimation.position(:,2),'-b',result.GroundTruth.time,result.GroundTruth.pos(:,2),'--r');
    ylabel('Location East (m)');
    nexttile(tcl)
    plot(result.IMU.time,result.Estimation.position(:,3),'-b',result.GroundTruth.time,result.GroundTruth.pos(:,3),'--r');
    ylabel('Location Down (m)');
    xlabel('time(s)');
    hL = legend({'Estimation','Ground Truth'},'Orientation','horizontal'); 
    hL.Layout.Tile = 'South';
    linkaxes(findobj(f1,'type','axes'),'x');
    
    
    
    
    
end



function angles = attitude_measurement(a,m)
phi = atan2(a(2),a(3));
theta = atan2(-a(1),sqrt(a(2)^2+a(3)^2));
s_phi = sin(phi);c_phi = cos(phi);
s_theta = sin(theta);c_theta = cos(theta);
psi = atan2( m(3)*s_phi - m(2)*c_phi, m(1)*c_theta + ( m(2)*s_phi + m(3)*c_phi )*s_theta);
angles = [phi,theta,psi];
end

