
% %% Define your points
% point(1).waypoint = [0,0,0]';
% point(1).attitude = eul2quat([0,0,0])';%ZYX
% point(1).time = 0;
% 
% point(2).waypoint = [0,0,0]';
% point(2).attitude = eul2quat([0,0,pi/4])';%ZYX
% point(2).time = 1;
% 
% point(3).waypoint = [0,0,0]';
% point(3).attitude = eul2quat([0,0,-pi/4])';%ZYX
% point(3).time = 2;
% 
% point(4).waypoint = [0,0,0]';
% point(4).attitude = eul2quat([0,0,0])';%ZYX
% point(4).time = 3;
% 
% point(5).waypoint = [0,0,0]';
% point(5).attitude = eul2quat([0,pi/4,0])';%ZYX
% point(5).time = 4;
% 
% point(6).waypoint = [0,0,0]';
% point(6).attitude = eul2quat([0,-pi/4,0])';%ZYX
% point(6).time = 5;
% 
% point(7).waypoint = [0,0,0]';
% point(7).attitude = eul2quat([0,0,0])';%ZYX
% point(7).time = 6;
% 
% point(8).waypoint = [0,0,0]';
% point(8).attitude = eul2quat([0,0,0])';%ZYX
% point(8).time = 7;
% 
% point(9).waypoint = [0,0,0]';
% point(9).attitude = eul2quat([pi/2,0,0])';%ZYX
% point(9).time = 8;
% 
% point(10).waypoint = [0,0,0]';
% point(10).attitude = eul2quat([-pi/2,0,0])';%ZYX
% point(10).time = 9;
% 
% point(11).waypoint = [0,0,0]';
% point(11).attitude = eul2quat([0,0,0])';%ZYX
% point(11).time = 10;
% 
% point(12).waypoint = [0,0,0]';
% point(12).attitude = eul2quat([-pi/4,pi/4,pi/4])';%ZYX
% point(12).time = 11;
% 
% point(13).waypoint = [0,0,0]';
% point(13).attitude = eul2quat([pi/4,pi/4,-pi/4])';%ZYX
% point(13).time = 12;
% 
% point(14).waypoint = [0,0,0]';
% point(14).attitude = eul2quat([3*pi/4,-pi/4,pi/4])';%ZYX
% point(14).time = 13;
% 
% point(15).waypoint = [0,0,0]';
% point(15).attitude = eul2quat([-3*pi/4,-pi/4,-pi/4])';%ZYX
% point(15).time = 14;
% 
% point(16).waypoint = [0,0,0]';
% point(16).attitude = eul2quat([0,0,0])';%ZYX
% point(16).time = 15;

%% Define your points
point(1).waypoint = [0,0,0]';
point(1).attitude = eul2quat([0,0,0])';%ZYX
point(1).time = 0;

point(2).waypoint = [2,0.1,0]';
point(2).attitude = eul2quat([0,0,-pi/6])';%ZYX
point(2).time = 2;

point(3).waypoint = [4,-0.1,0]';
point(3).attitude = eul2quat([0,0,pi/6])';%ZYX
point(3).time = 4;

point(4).waypoint = [6,0.1,0]';
point(4).attitude = eul2quat([0,0,-pi/6])';%ZYX
point(4).time = 6;

point(5).waypoint = [8,-0.1,0]';
point(5).attitude = eul2quat([0,0,pi/6])';%ZYX
point(5).time = 8;

point(6).waypoint = [10,0,0]';
point(6).attitude = eul2quat([0,0,0])';%ZYX
point(6).time = 10;



%% some plot
figure
eulerPoints = eulerd(quaternion([point.attitude]'),'ZYX','frame');
scatter([point.time]',eulerPoints(:,1));
hold on
scatter([point.time]',eulerPoints(:,2));
hold on
scatter([point.time]',eulerPoints(:,3));

figure
temp = [point.waypoint]';
scatter3(temp(:,1),temp(:,2),temp(:,3))

%% making trajectory
trajectory = waypointTrajectory([point.waypoint]',[point.time],'Orientation',quaternion([point.attitude]'));
trajectory.SampleRate = 100;











