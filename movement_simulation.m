clear

file = uigetfile('*.mat','Select a trajectory file');
load(file);
states_log = trajectory_run(trajectory);
sim_data = imu_data_from_log(states_log);
sim_data.GT = states_log;
sim_data.Traj = trajectory;

