function imu_data = imu_data_from_log(log)
% you can change the IMU characteristics in IMU object

accel_specs = accelparams( ...
    'ConstantBias',0.4, ...
    'NoiseDensity',0.04, ...
    'BiasInstability',0);

gyro_specs = gyroparams( ...
    'ConstantBias',0.16, ...
    'NoiseDensity',0.01, ...
    'BiasInstability',0);

mag_specs = magparams( ...
    'ConstantBias',1, ...
    'NoiseDensity',0.3, ...
    'BiasInstability',0);   

IMU = imuSensor('accel-gyro-mag','Accelerometer', accel_specs,'Gyroscope', gyro_specs,'Magnetometer', mag_specs);
IMU.MagneticField = [22,0,38];%Magnetic field in out town neglecting declination

 

GPS = gpsSensor();
GPS.SampleRate = 5;
GPS.HorizontalPositionAccuracy = 0;
GPS.VerticalPositionAccuracy = 0;
GPS.VelocityAccuracy = 0;
GPS.DecayFactor = 1;




[accelReadings,gyroReadings,magReadings] = IMU(log.FreeAccl,log.angVel,log.orientation);

[GPSReadings,velReadings,~,~] = GPS(log.pos,log.vel);

imu_data.time = log.time;
imu_data.acc = accelReadings;
imu_data.gyro = gyroReadings;
imu_data.mag = magReadings;
imu_data.GPS = GPSReadings;
plot_imu_data(imu_data);

end

function plot_imu_data(imu_data)
    f1 = figure;
    tcl = tiledlayout(3,1);
    nexttile(tcl)
    plot(imu_data.time,imu_data.acc,'LineWidth',1)
    ylabel('acceleration (m/s2)');
    nexttile(tcl)
    plot(imu_data.time,imu_data.gyro,'LineWidth',1)
    ylabel('angular rate (rad/s)');
    nexttile(tcl)
    plot(imu_data.time,imu_data.mag,'LineWidth',1)
    ylabel('magnetic field (uT)');
    xlabel('time(s)');
    hL = legend({'x','y','z'},'Orientation','horizontal'); 
    hL.Layout.Tile = 'South';
    linkaxes(findobj(f1,'type','axes'),'x');
    
end