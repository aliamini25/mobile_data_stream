---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements

# Embedded files
4842c26ef935905fbc530f94468edf5ae96fd6e8: [[Pasted Image 20230218114253_063.png]]
07bba9cbfe89926a18fa841ff0a10ff38a071bd7: [[Pasted Image 20230218114323_069.png]]

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://excalidraw.com",
	"elements": [
		{
			"id": "XaKd0KHSUEI74OGm4OkD7",
			"type": "image",
			"x": -410.267523492661,
			"y": -188.1088770132186,
			"width": 272.71835050531695,
			"height": 621.1917983732219,
			"angle": 0,
			"strokeColor": "transparent",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"roundness": null,
			"seed": 786986391,
			"version": 111,
			"versionNonce": 114029111,
			"isDeleted": false,
			"boundElements": [
				{
					"id": "uf8O1qqFcIhuoB99NNlX3",
					"type": "arrow"
				}
			],
			"updated": 1676708024899,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "4842c26ef935905fbc530f94468edf5ae96fd6e8",
			"scale": [
				1,
				1
			]
		},
		{
			"id": "Lv1a4mUjmCB8A2yAyxRST",
			"type": "image",
			"x": 69.53166184055192,
			"y": -185.83390574057074,
			"width": 267.1287149708196,
			"height": 608.4598507668669,
			"angle": 0,
			"strokeColor": "transparent",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"roundness": null,
			"seed": 1736122327,
			"version": 188,
			"versionNonce": 219382713,
			"isDeleted": false,
			"boundElements": [
				{
					"id": "uf8O1qqFcIhuoB99NNlX3",
					"type": "arrow"
				}
			],
			"updated": 1676708056133,
			"link": null,
			"locked": false,
			"status": "pending",
			"fileId": "07bba9cbfe89926a18fa841ff0a10ff38a071bd7",
			"scale": [
				1,
				1
			]
		},
		{
			"id": "uf8O1qqFcIhuoB99NNlX3",
			"type": "arrow",
			"x": -113.35267389653472,
			"y": 58.912994301048116,
			"width": 157.90954938975798,
			"height": 21.559440772571563,
			"angle": 0,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"roundness": {
				"type": 2
			},
			"seed": 1889934905,
			"version": 80,
			"versionNonce": 581070935,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1676708056133,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					81.05099463577926,
					-19.56404421713262
				],
				[
					157.90954938975798,
					1.9953965554389441
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "XaKd0KHSUEI74OGm4OkD7",
				"focus": -0.07225167688340652,
				"gap": 24.19649909080931
			},
			"endBinding": {
				"elementId": "Lv1a4mUjmCB8A2yAyxRST",
				"focus": 0.03809273633326526,
				"gap": 24.97478634732863
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#2b8a3e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 4,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 882.8126814701563,
		"scrollY": 477.63472452587064,
		"zoom": {
			"value": 0.5724793955036749
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"colorPalette": {},
		"currentStrokeOptions": null,
		"previousGridSize": null
	},
	"files": {}
}
```
%%